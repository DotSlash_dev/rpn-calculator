package com.dotslash.rpn_calculator;
import com.dotslash.rpn_calculator.R;

import java.lang.Double;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class Operation
{
    RPNActivity activity;
    int membersNb;
    String calcMethodName;
    Method calcMethod;
    ArrayList<String> validations;
    Button button;
    ArrayAdapter<Double> stack;

    public Operation(final RPNActivity activity, String name, int membersNb, final String method)
    {
        Class[] args = {double[].class};
        ViewGroup layout = (ViewGroup) activity.findViewById(R.id.operations);

        this.validations = new ArrayList<String>();
        this.activity = activity;
        this.stack = activity.getAdapter();
        button = new Button(activity);
        this.calcMethodName = method;
        this.membersNb = membersNb;
        try
        {
            this.calcMethod = Operation.class.getMethod(calcMethodName, args);
            button.setText(name);
            button.setEnabled(false);
            button.setLayoutParams(new ViewGroup.LayoutParams(60, -1));
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    calc();
                }
            });
            layout.addView(button);
        }
        catch (Exception e) { System.out.println(e.toString()); }
    }

    public Button getButton() { return button; }
    public String getName() { return calcMethodName; }
    public void addValidation(String validation) { this.validations.add(validation); }

    private void calc()
    {
        double result = 0;

        double[] nbs = new double[membersNb];
        for (int i = membersNb - 1; i >= 0; i--)
        {
            nbs[i] = stack.getItem(0);
            stack.remove(nbs[i]);
        }

        try
        {
            result = (Double) calcMethod.invoke(null, nbs);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
            result = Double.NaN;
        }
        finally
        {
            stack.insert(result, 0);
        }

        activity.enableAllOps();
    }

    public void enable()
    {
        boolean enable = true;
        if (stack.getCount() == 0)
            enable = false;
        else
        {
            double firstElem = stack.getItem(0);
            boolean[] checks = {
                stack.getCount() >= membersNb,
                !Double.isInfinite(firstElem),
                !Double.isNaN(firstElem)
            };

            for (boolean check : checks)
                if (!check)
                {
                    enable = false;
                    break;
                }
            if (enable && !validations.isEmpty())
            {
                Iterator<String> it = validations.iterator();
                while (it.hasNext())
                {
                    String validationName = it.next();
                    try
                    {
                        Method validation = Operation.class.getMethod(validationName);
                        if ((Boolean) validation.invoke(this) == false)
                        {
                            enable = false;
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        enable = false;
                        break;
                    }
                }
            }
        }
        button.setEnabled(enable);
    }

    public static double add(double[] nbs) { return nbs[0] + nbs[1]; }
    public static double sub(double[] nbs) { return nbs[0] - nbs[1]; }
    public static double mult(double[] nbs) { return nbs[0] * nbs[1]; }
    public static double div(double[] nbs) { return nbs[0] / nbs[1]; }
    public static double mod(double[] nbs) { return nbs[0] % nbs[1]; }
    public static double inv(double[] nbs) { return 1 / nbs[0]; }
    public static double sqr(double[] nbs) { return Math.pow(nbs[0], 2); }
    public static double sqrt(double[] nbs) { return Math.sqrt(nbs[0]); }
    public static double pow(double[] nbs) { return Math.pow(nbs[0], nbs[1]); }
    public static double n_rt(double[] nbs) { return Math.pow(nbs[0], 1.0 / nbs[1]); }
    public static double sin(double[] nbs) { return Math.sin(nbs[0]); }
    public static double asin(double[] nbs) { return Math.asin(nbs[0]); }
    public static double cos(double[] nbs) { return Math.cos(nbs[0]); }
    public static double acos(double[] nbs) { return Math.acos(nbs[0]); }
    public static double tan(double[] nbs) { return Math.tan(nbs[0]); }
    public static double atan(double[] nbs) { return Math.atan(nbs[0]); }

    public boolean isPositive() { return stack.getItem(0) >= 0; }
    public boolean isNot0() { return stack.getItem(0) != 0; }
    public boolean cosineNot0() { return Math.cos(stack.getItem(0)) != 0; }
    public boolean bewteenMin1And1()
    {
      double stacked_value = stack.getItem(0);
      return (stacked_value >= -1 && stacked_value <= 1);
    }
}
