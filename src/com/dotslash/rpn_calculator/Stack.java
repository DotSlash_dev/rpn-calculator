package com.dotslash.rpn_calculator;
import com.dotslash.rpn_calculator.R;

import java.lang.Double;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

class StackAdapter extends ArrayAdapter<Double>
{
    Context context;

    public StackAdapter(Context context, int resourceId)
    {
        super(context, resourceId);

        this.context = context;
    }

    public View getView(int position, View view, ViewGroup parent)
    {
        if (view == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.stack, null);
        }
        double number = getItem(position);
        TextView tv = (TextView) view.findViewById(R.id.nbr);
        tv.setTextSize(10);
        tv.setGravity(0x11);
        tv.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
        tv.setText(String.valueOf(number));
        return view;
    }
}
