package com.dotslash.rpn_calculator;

import java.lang.Math;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class RPNActivity extends Activity
{
    Operation[] operations;

    private void setOps()
    {

        try
        {
            Operation[] ops = {
                new Operation(this, "+", 2, "add"),
                new Operation(this, "-", 2, "sub"),
                new Operation(this, "*", 2, "mult"),
                new Operation(this, "/", 2, "div"),
                new Operation(this, "%", 2, "mod"),
                new Operation(this, "1/x", 1, "inv"),
                new Operation(this, "x^2", 1, "sqr"),
                new Operation(this, "sqrt", 1, "sqrt"),
                new Operation(this, "x^y", 2, "pow"),
                new Operation(this, "n-rt", 2, "n_rt"),
                new Operation(this, "sin", 1, "sin"),
                new Operation(this, "asin", 1, "asin"),
                new Operation(this, "cos", 1, "cos"),
                new Operation(this, "acos", 1, "acos"),
                new Operation(this, "tan", 1, "tan"),
                new Operation(this, "atan", 1, "atan")
            };
            this.operations = ops;
        }
        catch (Exception e)
        {
            System.out.println("Can not create operation : " + e);
        }
    }

    private Operation findOperationbyName(String name)
    {
        for (Operation operation : operations)
            if (name == operation.getName())
                return operation;
        return null;
    }

    private void setValidations()
    {
        findOperationbyName("div").addValidation("isNot0");
        findOperationbyName("mod").addValidation("isNot0");
        findOperationbyName("inv").addValidation("isNot0");
        findOperationbyName("sqrt").addValidation("isPositive");
        findOperationbyName("n_rt").addValidation("isPositive");
        findOperationbyName("n_rt").addValidation("isNot0");
        findOperationbyName("tan").addValidation("cosineNot0");
        findOperationbyName("asin").addValidation("bewteenMin1And1");
        findOperationbyName("acos").addValidation("bewteenMin1And1");
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        displayDigitButtons();
        ListView stack = (ListView) findViewById(R.id.stack);
        stack.setAdapter(new StackAdapter(this, R.layout.stack));
        setOps();
        setValidations();
    }

    ArrayAdapter<Double> getAdapter()
    {
        ListView stack = (ListView) findViewById(R.id.stack);
        return ((ArrayAdapter<Double>) stack.getAdapter());
    }

    public void enableAllOps()
    {
        for (Operation op : operations)
            op.enable();
    }

    public void addDigit(View v)
    {
        Button button = (Button) findViewById(v.getId());
        TextView textView = (TextView) findViewById(R.id.number);
        String text = button.getText().toString();

        textView.setText(textView.getText() + text);
    }

    private void setNumButtonOptions(final Button button, String label)
    {
        LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, (float) 1);

        button.setText(label);
        button.setTextSize(24);
        button.setLayoutParams(layoutParam);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView textView = (TextView) findViewById(R.id.number);
                String oldText = textView.getText().toString();
                String text = button.getText().toString();

                textView.setText(oldText + text);
                for (Operation op : operations)
                    op.getButton().setEnabled(false);
            }
        });
    }

    private void displayDigitButtons()
    {
        String[][] buttonLabels = {
            {"1","2","3"},
            {"4","5","6"},
            {"7","8","9"},
            {"-","0","."}
        };
        LinearLayout digitLayout = (LinearLayout) findViewById(R.id.digit_buttons);

        for (String[] labelGroup : buttonLabels)
        {
            LinearLayout layout = new LinearLayout(this);
            layout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));

            for (String label : labelGroup)
            {
                Button button = new Button(this);
                setNumButtonOptions(button, label);
                layout.addView(button);
            }
            digitLayout.addView(layout);
        }
    }

    public void store(View v)
    {
        TextView number_edit = (TextView) findViewById(R.id.number);
        ListView stack = (ListView) findViewById(R.id.stack);
        String nb = number_edit.getText().toString();
        TextView error = (TextView) findViewById(R.id.error_field);

        error.setText("");

        if (!nb.isEmpty() && nb.matches(getString(R.string.number_regex)))
        {
            ((ArrayAdapter) stack.getAdapter()).insert(Double.parseDouble(nb), 0);

            enableAllOps();
        }
        else if (nb.isEmpty())
            error.setText(getString(R.string.error_no_number));
        else
            error.setText(getString(R.string.error_not_valid_number));
        number_edit.setText("");
    }

    public void correct(View view)
    {
        TextView number_edit = (TextView) findViewById(R.id.number);
        String oldText = number_edit.getText().toString();
        int length = oldText.length();

        if (length != 0)
        {
            number_edit.setText(oldText.substring(0, length - 1));
            if (number_edit.getText().length() == 0)
                enableAllOps();
        }
    }

    public void clean(View view)
    {
        TextView number_edit = (TextView) findViewById(R.id.number);

        number_edit.setText("");
        enableAllOps();
    }

    private double popDoubleFromStack(LinearLayout stack)
    {
        if (stack.getChildCount() == 0)
            return Double.NaN;

        TextView textView = (TextView) stack.getChildAt(0);
        double nb = Double.parseDouble(textView.getText().toString());
        stack.removeViewAt(0);

        return nb;
    }

    public void resetStack(View v)
    {
        getAdapter().clear();
        enableAllOps();
    }

    public void removeFromStack(View v)
    {
        ArrayAdapter<Double> adapter = getAdapter();
        adapter.remove(adapter.getItem(0));
        enableAllOps();
    }

    public void showHelp(View view)
    {
        Intent intent = new Intent(this, HelpActivity.class);
        intent.setAction("com.dotslash.rpn_calculator.HELP");
        startActivity(intent);
    }
}
